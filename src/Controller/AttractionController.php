<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AttractionController extends AbstractController
{
    /**
     * @Route("/attractions", name="attractions")
     */
    public function index()
    {
        return $this->render('attraction/attractions.html.twig', [
            'controller_name' => 'AttractionController',
        ]);
    }
}
