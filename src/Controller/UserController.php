<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home(Request $request){
        $locale = $request->getLocale();
        return $this->render('home.html.twig');
    }

    /**
     * @Route("/compte/{id}/edit", name="edit_user")
     */
    public function form(User $user = null, Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder($user)
                     ->add('email')
                     ->add('username')
                     ->add('name')
                     ->add('surname')
                     ->add('city')
                     ->getForm();

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('home', ['id' => $user->getId()]);
        }

        return $this->render('security/account_edit.html.twig', [
            'formUser' => $form->createView(),
            'user' => $user
        ]);
    }

    // /**
    //  * @Route("/compte/{id}", name="account")
    //  */
    // public function show(UserRepository $repo, $id)
    // {
    //     $user = $repo->find($id);

    //     return $this->render('security/account.html.twig', [
    //         'user' => $user
    //     ]);
    // }

    /**
     * @Route("/compte", name="account")
     */
    public function index()
    {
        return $this->render('security/account.html.twig', [
        ]);
    }

}
