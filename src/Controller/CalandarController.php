<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CalandarController extends AbstractController
{
    /**
     * @Route("/calandar", name="calandar")
     */
    public function index()
    {
        return $this->render('calandar/index.html.twig', [
            'controller_name' => 'CalandarController',
        ]);
    }
}
