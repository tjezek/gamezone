<?php

namespace App\Controller;

use App\Repository\TicketRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdministrationController extends AbstractController
{
    /**
     * @Route("/administration", name="administration")
     */
    public function index(TicketRepository $repo)
    {
        $ticket = $repo->findAll();

        return $this->render('administration/index.html.twig', [
            'controller_name' => 'AdministrationController',
            'ticket' => $ticket
        ]);
    }
}
