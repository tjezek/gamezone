<?php

namespace App\Controller;

use App\Entity\Ticket;
use App\Repository\TicketRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class TicketController extends AbstractController
{
    /**
     * @Route("/ticket", name="ticket")
     */
    public function index(TicketRepository $repo)
    {

        $ticket = $repo->findAll();

        return $this->render('ticket/index.html.twig', [
            'controller_name' => 'TicketController',
            'ticket' => $ticket
        ]);
    }

      /**
     * @Route("/ticket/{id}/edit", name="edit_ticket")
     */
    public function form(Ticket $ticket = null, Request $request, ObjectManager $manager)
    {
        $form = $this->createFormBuilder($ticket)
                     ->add('name')
                     ->add('description')
                     ->add('price')
                     ->getForm();

        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($ticket);
            $manager->flush();

            return $this->redirectToRoute('administration');
        }

        return $this->render('ticket/edit.html.twig', [
            'formTicket' => $form->createView()
        ]);
    }
}
