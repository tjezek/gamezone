<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LanguageController extends AbstractController
{
    /**
     * @Route("/setlocale/{language}", name="setlocale")
     */
    public function setLocaleAction(Request $request, $language = null)
    {
        if ($language != null) {
            $this->get('session')->set('_locale', $language);
        }
        
        $url = $request->headers->get('referer');
        if (empty($url)) {
            $url = $this->container->get('router')->generate('home');
        }

        return $this->redirect($url);
    }
}
