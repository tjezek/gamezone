Element.prototype.remove = function () {
    this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function () {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] && this[i].parentElement) {
            this[i].parentElement.removeChild(this[i]);
        }
    }
}

function myRestaurants() {

    var checkBox = document.getElementById("restaurants");

    if (checkBox.checked == true) {

        var newDiv = document.createElement('div');
        newDiv.style.top = "125px";
        newDiv.style.right = "175px";
        newDiv.className = 'cursor_purple';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "65px";
        newDiv.style.right = "345px";
        newDiv.className = 'cursor_purple';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "265px";
        newDiv.style.right = "215px";
        newDiv.className = 'cursor_purple';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "340px";
        newDiv.style.right = "480px";
        newDiv.className = 'cursor_purple';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "290px";
        newDiv.style.right = "660px";
        newDiv.className = 'cursor_purple';
        document.getElementById('myPlan').appendChild(newDiv);

    } else {
        document.getElementsByClassName("cursor_purple").remove();
    }
}

function myBoutiques() {

    var checkBox = document.getElementById("boutiques");

    if (checkBox.checked == true) {
        var newDiv = document.createElement('div');
        newDiv.style.top = "240px";
        newDiv.style.right = "100px";
        newDiv.className = 'cursor_blue';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "180px";
        newDiv.style.right = "290px";
        newDiv.className = 'cursor_blue';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "90px";
        newDiv.style.right = "290px";
        newDiv.className = 'cursor_blue';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "280px";
        newDiv.style.right = "410px";
        newDiv.className = 'cursor_blue';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "385px";
        newDiv.style.right = "610px";
        newDiv.className = 'cursor_blue';
        document.getElementById('myPlan').appendChild(newDiv);

    } else {
        document.getElementsByClassName("cursor_blue").remove();
    }
}

function myPhotos() {

    var checkBox = document.getElementById("photos");

    if (checkBox.checked == true) {
        var newDiv = document.createElement('div');
        newDiv.style.top = "160px";
        newDiv.style.right = "100px";
        newDiv.className = 'cursor_yellow';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "200px";
        newDiv.style.right = "240px";
        newDiv.className = 'cursor_yellow';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "130px";
        newDiv.style.right = "330px";
        newDiv.className = 'cursor_yellow';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "160px";
        newDiv.style.right = "420px";
        newDiv.className = 'cursor_yellow';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "290px";
        newDiv.style.right = "550px";
        newDiv.className = 'cursor_yellow';
        document.getElementById('myPlan').appendChild(newDiv);
    } else {
        document.getElementsByClassName("cursor_yellow").remove();
    }
}


function myServices() {

    var checkBox = document.getElementById("services");

    if (checkBox.checked == true) {
        var newDiv = document.createElement('div');
        newDiv.style.top = "190px";
        newDiv.style.right = "170px";
        newDiv.className = 'cursor_pink';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "290px";
        newDiv.style.right = "250px";
        newDiv.className = 'cursor_pink';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "235px";
        newDiv.style.right = "520px";
        newDiv.className = 'cursor_pink';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "170px";
        newDiv.style.right = "530px";
        newDiv.className = 'cursor_pink';
        document.getElementById('myPlan').appendChild(newDiv);

        var newDiv = document.createElement('div');
        newDiv.style.top = "310px";
        newDiv.style.right = "440px";
        newDiv.className = 'cursor_pink';
        document.getElementById('myPlan').appendChild(newDiv);
    } else {
        document.getElementsByClassName("cursor_pink").remove();
    }
}

function myAttractions() {
    var selectione = "";

    for (i = 0; i < document.forms.f.size.options.length; i++) {
        if (document.forms.f.size.options[i].selected) {
            selectione = selectione + document.forms.f.size.options[i].text;
        }
    }
    if (selectione <= '109') {

        document.getElementsByClassName("cursor_att").remove();

        // BattleKart
        var newDiv = document.createElement('div');
        newDiv.style.top = "380px";
        newDiv.style.right = "360px";
        newDiv.style.backgroundImage = "url('../img/logo_battle_kart.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // GameCenter
        var newDiv = document.createElement('div');
        newDiv.style.top = "240px";
        newDiv.style.right = "690px";
        newDiv.style.backgroundImage = "url('../img/logo_game_center.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Heroes Team
        var newDiv = document.createElement('div');
        newDiv.style.top = "200px";
        newDiv.style.right = "0px";
        newDiv.style.backgroundImage = "url('../img/logo_heroes.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Champions League
        var newDiv = document.createElement('div');
        newDiv.style.top = "300px";
        newDiv.style.right = "80px";
        newDiv.style.backgroundImage = "url('../img/logo_champions_league.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);
    } else if (selectione >= '110' && selectione <= '129') {

        document.getElementsByClassName("cursor_att").remove();

        // BattleKart
        var newDiv = document.createElement('div');
        newDiv.style.top = "380px";
        newDiv.style.right = "360px";
        newDiv.style.backgroundImage = "url('../img/logo_battle_kart.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // GameCenter
        var newDiv = document.createElement('div');
        newDiv.style.top = "240px";
        newDiv.style.right = "690px";
        newDiv.style.backgroundImage = "url('../img/logo_game_center.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Champions League Survivor
        var newDiv = document.createElement('div');
        newDiv.style.top = "100px";
        newDiv.style.right = "450px";
        newDiv.style.backgroundImage = "url('../img/logo_champions_league_survivor.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Super Fighter League
        var newDiv = document.createElement('div');
        newDiv.style.top = "50px";
        newDiv.style.right = "200px";
        newDiv.style.backgroundImage = "url('../img/logo_fighter_league.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Heroes Team
        var newDiv = document.createElement('div');
        newDiv.style.top = "200px";
        newDiv.style.right = "0px";
        newDiv.style.backgroundImage = "url('../img/logo_heroes.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Champions League
        var newDiv = document.createElement('div');
        newDiv.style.top = "300px";
        newDiv.style.right = "80px";
        newDiv.style.backgroundImage = "url('../img/logo_champions_league.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);
    } else {

        document.getElementsByClassName("cursor_att").remove();

        // BattleKart
        var newDiv = document.createElement('div');
        newDiv.style.top = "380px";
        newDiv.style.right = "360px";
        newDiv.style.backgroundImage = "url('../img/logo_battle_kart.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Fighter Hard Team
        var newDiv = document.createElement('div');
        newDiv.style.top = "360px";
        newDiv.style.right = "650px";
        newDiv.style.backgroundImage = "url('../img/logo_fighter_hard.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // GameCenter
        var newDiv = document.createElement('div');
        newDiv.style.top = "240px";
        newDiv.style.right = "690px";
        newDiv.style.backgroundImage = "url('../img/logo_game_center.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Awesome Heroes Team
        var newDiv = document.createElement('div');
        newDiv.style.top = "200px";
        newDiv.style.right = "560px";
        newDiv.style.backgroundImage = "url('../img/logo_awesome_heroes.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Champions League Survivor
        var newDiv = document.createElement('div');
        newDiv.style.top = "100px";
        newDiv.style.right = "450px";
        newDiv.style.backgroundImage = "url('../img/logo_champions_league_survivor.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Super Fighter League
        var newDiv = document.createElement('div');
        newDiv.style.top = "50px";
        newDiv.style.right = "200px";
        newDiv.style.backgroundImage = "url('../img/logo_fighter_league.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Contagion VR
        var newDiv = document.createElement('div');
        newDiv.style.top = "50px";
        newDiv.style.right = "50px";
        newDiv.style.backgroundImage = "url('../img/logo_contagion_vr.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Heroes Team
        var newDiv = document.createElement('div');
        newDiv.style.top = "200px";
        newDiv.style.right = "0px";
        newDiv.style.backgroundImage = "url('../img/logo_heroes.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);

        // Champions League
        var newDiv = document.createElement('div');
        newDiv.style.top = "300px";
        newDiv.style.right = "80px";
        newDiv.style.backgroundImage = "url('../img/logo_champions_league.png')";
        newDiv.className = 'cursor_att';
        document.getElementById('myPlan').appendChild(newDiv);
    }
}